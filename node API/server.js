const axios = require('axios');
const querystring = require('querystring');
const fs = require('fs');

const api_key = "4da6b754ca6432eb334c7ef9fe9e3e8b"
const folder = "C:\\Users\\yacin\\Desktop\\projet de recherche\\addic7ed"

series = fs.readdirSync(folder)
new_series = []
for (let s of series){
    if (  s.includes(".txt") ){
        continue;
    }else{
        s = s.replace(/[_]+|^[\d]+|.txt$|\([\w]*\)|BBC/g, " ").trim()   
        new_series.push(s)
    }
}
for (let s of new_series){
console.log(s)
}

var writeStream = fs.createWriteStream("genre_data.csv", {flags :  'a'})
writeStream.write("id,genre\n")
let dicGenre = {}
axios.get("https://api.themoviedb.org/3/genre/tv/list?"+querystring.stringify({ api_key: api_key})).then((res)=>{
    for (let g of res.data["genres"] ){
        dicGenre[g.id] = g.name
        writeStream.write( `${g.id},${g.name}\n`)
    }
    writeStream.end();
}).catch(function (error) {
    // handle error
    console.log(error);
})

wStream = fs.createWriteStream("series_data.csv", {flags : "a"})
wStream.write("id,name,nom_repertoire,genreID,popularite\n");

for (let i = 0; i<new_series.length; i++){
    setTimeout(()=>{
        axios.get("https://api.themoviedb.org/3/search/tv?"+querystring.stringify({ api_key: api_key, query :new_series[i]})).then(function (response) {
            // handle success

            let serie = response.data["results"][0];
            console.log(response.data["results"].length)
            if (! response.data["results"].length){
                console.log(new_series[i])                
                console.log(`\nCouldn't get data for  :  ${new_series[i]}\n`);
            }else{
                console.log(new_series[i])
                //console.log("********** "+dicGenre[serie["genre_ids"][0]]);
                wStream.write(`${serie.id},${serie["name"].replace(/,/g, "")},${series[i]},${serie["genre_ids"][0]},${serie.popularity}\n`)
            }
           

        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
    }, i*100)
}
//wStream.end();

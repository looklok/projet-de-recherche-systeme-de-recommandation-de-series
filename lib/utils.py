import numpy as np
import pandas as pd
import copy

def euclidian_distance (x , y):
    return np.sqrt(np.sum((y-x)**2))
    
def cosine_distance(x, y) : 
    produitNorme=np.sqrt(np.sum((x)**2))*np.sqrt(np.sum((y)**2))
    return np.dot(x,y)/produitNorme
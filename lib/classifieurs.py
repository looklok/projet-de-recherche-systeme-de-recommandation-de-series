import numpy as np
import pandas as pd
import copy

class Classifier:
    """ Classe pour représenter un classifieur
        Attention: cette classe est une classe abstraite, elle ne peut pas être
        instanciée.
    """
    
    def __init__(self, input_dimension):
        """ Constructeur de Classifier
            Argument:
                - intput_dimension (int) : dimension de la description des exemples
            Hypothèse : input_dimension > 0
        """
        raise NotImplementedError("Please Implement this method")
        
    def train(self, desc_set, label_set):
        """ Permet d'entrainer le modele sur l'ensemble donné
            desc_set: ndarray avec des descriptions
            label_set: ndarray avec les labels correspondants
            Hypothèse: desc_set et label_set ont le même nombre de lignes
        """        
        raise NotImplementedError("Please Implement this method")
    
    def score(self,x):
        """ rend le score de prédiction sur x (valeur réelle)
            x: une description
        """
        raise NotImplementedError("Please Implement this method")
    
    def predict(self, x):
        """ rend la prediction sur x (soit -1 ou soit +1)
            x: une description
        """
        raise NotImplementedError("Please Implement this method")

    def accuracy(self, desc_set, label_set):
        """ Permet de calculer la qualité du système sur un dataset donné
            desc_set: ndarray avec des descriptions
            label_set: ndarray avec les labels correspondants
            Hypothèse: desc_set et label_set ont le même nombre de lignes
        """
        # COMPLETER CETTE FONCTION ICI : 
        # ............
        # ............
        pred_labels = np.zeros(label_set.shape)
        for i in range(desc_set.shape[0]):
            pred_labels[i] = self.predict(desc_set[i, :])
        return sum(pred_labels == label_set) / len(label_set)


class ClassifierPerceptron(Classifier):
    """ Perceptron de Rosenblatt
    """
    
    def __init__(self, input_dimension,learning_rate):
        """ Constructeur de Classifier
            Argument:
                - input_dimension (int) : dimension de la description des exemples
                - learning_rate : epsilon
            Hypothèse : input_dimension > 0
        """
        self.epsilon = learning_rate
        self.w = np.random.uniform(-1,1, input_dimension)
        
        
    def train(self, desc_set, label_set, verbose=False, steps=20):
        """ Permet d'entrainer le modele sur l'ensemble donné
            réalise une itération sur l'ensemble des données prises aléatoirement
            desc_set: ndarray avec des descriptions
            label_set: ndarray avec les labels correspondants
            verbose : conserver ou pas l'évolution de la précision
            Hypothèse: desc_set et label_set ont le même nombre de lignes
        """        
        convergence=False
        cpt = 0
        while not(convergence) and cpt <steps:
            cpt+=1
            indexx = np.arange(desc_set.shape[0])
            np.random.shuffle(indexx)
            acc = []
            w1 = self.w
            for ind in indexx :
                
                if verbose : 
                    acc.append(self.accuracy(desc_set, label_set))
                    
                s = self.predict(desc_set[ind, :])
                if s != label_set[ind] :
                    self.w = self.w + self.epsilon * label_set[ind] * desc_set[ind, :]
            
            if np.sqrt(np.sum((self.w-w1)**2)) < self.epsilon:
                    convergence = True
        
        if verbose :
                return (list(range(desc_set.shape[0])), acc)        # l'evolution de la precision par itération
    def score(self,x):
        """ rend le score de prédiction sur x (valeur réelle)
            x: une description
        """
        return self.w @ x
    
    
    def predict(self, x):
        """ rend la prediction sur x (soit -1 ou soit +1)
            x: une description
        """
        s = self.score(x)
        return -1 if s <0 else 1


class Perceptron_MC(Classifier):
    """ Perceptron de Rosenblatt
    """
    def __init__(self, input_dimension,learning_rate,nb_classes):
        """ Constructeur de Classifier
            Argument:
                - input_dimension (int) : dimension de la description des exemples
                - learning_rate : epsilon
            Hypothèse : input_dimension > 0
        """
        self.input_dimension=input_dimension
        self.learning_rate=learning_rate
        self.w= np.zeros((input_dimension, nb_classes))
        self.nb_classes=nb_classes
        
    def train(self, desc_set, label_set):
        """ Permet d'entrainer le modele sur l'ensemble donné
            réalise une itération sur l'ensemble des données prises aléatoirement
            desc_set: ndarray avec des descriptions
            label_set: ndarray avec les labels correspondants
            Hypothèse: desc_set et label_set ont le même nombre de lignes
        """
        #W=np.zeros((self.input_dimension,self.nb_classes))     
        for i in range(self.nb_classes):
            perceptron= ClassifierPerceptron(self.input_dimension,self.learning_rate)
            y_i=np.where(label_set==i,label_set,-1)
            y_i=np.where(y_i==-1,y_i,1)
            perceptron.train(desc_set,y_i)
            wi = perceptron.w
            if i == 0 :
                W = wi.reshape((-1,1))
            else:
                W=np.append(W,np.reshape(wi,(-1,1)),axis=1)
        self.w=W
        
        return W
                
    def score(self,x):
        """ rend le score de prédiction sur x (valeur réelle)
            x: une description
        """
        return np.dot(x,self.w)
        
    
    def predict(self, x):
        """ rend la prediction sur x (soit -1 ou soit +1)
            x: une description
        """
        return np.argmax(self.score(x))


class ClassifierKNN_MC(Classifier):
    """ Classe pour représenter un classifieur par K plus proches voisins.
        Cette classe hérite de la classe Classifier
    """

    #TODO: A Compléter
    
    def __init__(self, input_dimension, k, output_dimension, distance = "euclidian"):
        """ Constructeur de Classifier
            Argument:
                - intput_dimension (int) : dimension d'entrée des exemples
                - k (int) : nombre de voisins à considérer
                - output_dimension : dimension de sortie des exemples
            Hypothèse : input_dimension > 0
        """
        self.k = k
        self.output_dimension = output_dimension
        self.distance = distance
        
    def score(self,x):
        """ rend la proportion de +1 parmi les k ppv de x (valeur réelle)
            x: une description : un ndarray
        """
        dist = []
        if self.distance == "euclidian":
            for el in self.train_x : 
                dist.append(np.sqrt(np.sum((el-x)**2)))
            sorted_index = np.argsort(dist)
        elif self.distance == "cosine":
            tab=[]
            for el in self.train_x :
                produitNorme=np.sqrt(np.sum((x)*2))*np.sqrt(np.sum((el)*2))
                dist.append((np.vdot(x,el))/produitNorme)
                #np.reshape(wi,(256,1))
                #tab.append(cosine_similarity(np.reshape(x,(1,256)),np.reshape(i,(1,256)))[0][0])
            sorted_index =np.argsort(dist)[::-1]
        pred = np.zeros(self.output_dimension)
        for ind in sorted_index[:self.k]:
            pred[self.train_y[ind]] +=1
        return pred
        
    def predict(self, x):
        """ rend la prediction sur x (-1 ou +1)
            x: une description : un ndarray
        """
        s = self.score(x)
        return np.argmax(s)

    def train(self, desc_set, label_set):
        """ Permet d'entrainer le modele sur l'ensemble donné
            desc_set: ndarray avec des descriptions
            label_set: ndarray avec les labels correspondants
            Hypothèse: desc_set et label_set ont le même nombre de lignes
        """        
        self.train_x = desc_set
        self.train_y = label_set